## Name
Scale Detection

## Description
Small Python library for finding specific scale bars and rulers in an image and deriving image scale from it (pixels per cm, mm, ...)

## Requirements
This library requires following additional Python libraries installed to work:
NumPy,
OpenCV (cv2),
Pytesseract,
SciPy,
scikit-image (skimage)

## Usage
Check demo.py
