
# import the scale detection library
import scale_detection as sc 

detector = sc.ScaleDetector("barbar_10.tif.png") # path to an image

# result = detector.find_scale_bar_area()    only search for scale bars
# result = detector.find_ruler_area()    only search for rulers
result = detector.resolve_image_scale()    # both

if result is not None:
    scale_image_2d_array, units, pixels = result    
    # scale_image_2d_array - 2d_array grayscale image of the area from which was 
    #                    the image scale derived
    # units - cm, mm, ...
    # pixels - 1 <units> = <pixels> in the image
    print(units, pixels)
else:
    print("RESULT NOT FOUND") # scale detection failed
