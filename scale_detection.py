# !/bin/bash

"""
###########################################################################
#
#                         PV162 - Scale Detection
#
###########################################################################

"""

from sys import argv
from os.path import exists

import numpy as np
import cv2
from pytesseract import image_to_string

from scipy import ndimage
from skimage import filters
from math import floor, hypot


SCALE_BAR = 0
RULER = 1


class ScaleDetector:
    def __init__(self, path):
        img = cv2.imread(path)
        self.image = img
        height, width = img.shape[0], img.shape[1]
        
        self.MIN_EDGE_MAGNITUDE = 10
        
        # scale bar constants
        self.MIN_SCALE_BAR_AREA = 200
        self.MAX_SCALE_BAR_AREA = max(
            (width / 5) * (height / 2), (width / 2) * (height / 5))
        self.SCALE_BAR_WH_MIN_RATIO = 0.1
        self.SCALE_BAR_WH_MAX_RATIO = 0.4
        
        # ruler constants
        self.MIN_RULER_AREA = 1400
        
        top = img[:height // 3, ::]
        right = img[::, width // 3 * 2:]
        bottom = img[height // 3 * 2:, ::]
        left = img[::, :width // 3]
        
        if len(self.image.shape) > 2:
            top = cv2.cvtColor(top, cv2.COLOR_RGB2GRAY)
            right = cv2.cvtColor(right, cv2.COLOR_RGB2GRAY)
            bottom = cv2.cvtColor(bottom, cv2.COLOR_RGB2GRAY)
            left = cv2.cvtColor(left, cv2.COLOR_RGB2GRAY)
        
        self.parts = [cv2.GaussianBlur(cv2.rotate(left, cv2.ROTATE_90_COUNTERCLOCKWISE), (3, 3), 0),
                      cv2.GaussianBlur(top, (3, 3), 0),
                      cv2.GaussianBlur(cv2.rotate(right, cv2.ROTATE_90_CLOCKWISE), (3, 3), 0),
                      cv2.GaussianBlur(bottom, (3, 3), 0)]
        
    def find_scale_bar_area(self):
        promising_image_areas = []

        for part in self.parts:
            edge = cv2.Laplacian(part, ddepth=-1)            
            _, th = cv2.threshold(edge, self.MIN_EDGE_MAGNITUDE, 255, cv2.THRESH_BINARY)
            contours, _ = cv2.findContours(th, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

            for cnt in contours:
                (_, _), (w, h), angle_of_rotation = cv2.minAreaRect(cnt)
                if self.MIN_SCALE_BAR_AREA < w*h < self.MAX_SCALE_BAR_AREA:
                    wh_ratio = min(w, h) / max(w, h)
                    if self.SCALE_BAR_WH_MIN_RATIO < wh_ratio < self.SCALE_BAR_WH_MAX_RATIO:
                        y, x, W, H = cv2.boundingRect(cnt)
                        extracted = rotate_and_extract(edge[x:x+H, y:y+W], w, h, angle_of_rotation, SCALE_BAR)
                        promising_image_areas.append(extracted)

        for subimg in sorted(promising_image_areas, key=lambda x: x.shape[0]*x.shape[1], reverse=True):
            result = resolve_scale_bar_metric(subimg)
            if result is not None:
                scale_marked, units, scale = result
                return scale_marked, units, round(scale, 2)

        return None

    def find_ruler_area(self):
        promising_image_areas = []
        part_contours = []
        
            
        for part in self.parts:
            edge = filters.sobel(part)
            edge = np.uint8(edge * 255)
            
            _, th = cv2.threshold(edge, self.MIN_EDGE_MAGNITUDE, 255, cv2.THRESH_BINARY)
            dil = cv2.dilate(th, None) 
            
            contours, _ = cv2.findContours(dil, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
            part_contours.append(contours)
            
            for cnt in contours:
                (_, _), (w, h), rot_angle = cv2.minAreaRect(cnt)
                if w*h > self.MIN_RULER_AREA:
                    y, x, W, H = cv2.boundingRect(cnt)
                    extracted = rotate_and_extract(part[x:x+H, y:y+W], w, h, rot_angle, RULER)
                    promising_image_areas.append(extracted)
                
        for img_area in sorted(promising_image_areas, key=lambda x: x.shape[0]*x.shape[1], reverse=True):
            result = resolve_ruler_metric(img_area)
            if result is not None:
                return get_formatted_result(img_area, result)
            
            
        for i, cnt_list in enumerate(part_contours):
            part_size_limit = (self.parts[i].shape[0]*self.parts[i].shape[1]) // 1400
            possible_ruler_lines = []
            
            for cnt in cnt_list:
                (_, _), (w, h), angle_of_rotation = cv2.minAreaRect(cnt) 
                if w*h > part_size_limit:
                    possible_ruler_lines.append(cnt)
            
            result = sort_and_filter_ruler_contours(possible_ruler_lines, self.parts[i])
            if result is not None:
                return get_formatted_result(self.parts[i], result)

        return None

    def resolve_image_scale(self):
        result_scale_bar = self.find_scale_bar_area()
        if result_scale_bar is None:
            result_ruler = self.find_ruler_area()
            if result_ruler is None:
                return None
            
            scale_marked, units, pixels = result_ruler
            return scale_marked, units, pixels
        
        scale_marked, units, pixels = result_scale_bar
        return scale_marked, units, pixels


def get_formatted_result(img, result):
    fst, snd, units = result
    ruler_marked = mark_ruler_bar(img, fst, snd)
    (xl, yl), (_, _), _ = cv2.minAreaRect(fst)
    (xr, yr), (_, _), _ = cv2.minAreaRect(snd)
    distance = round(hypot(xr - xl, yr - yl))
    
    if units == "half in":
        distance *= 2
        units = "in"
        
    return ruler_marked, units, distance



def rotate_and_extract(img, w, h, angle_of_rotation, scale_type):
    if w > h:
        possible_scale = ndimage.rotate(img, angle_of_rotation)
    else:
        possible_scale = ndimage.rotate(img, angle_of_rotation - 90)
        w, h = h, w

    img_w, img_h = possible_scale.shape[1], possible_scale.shape[0]
    
    center_x, center_y = img_w // 2, img_h // 2
    half_w = floor(w) // 2
    half_h = floor(h) // 2

    if scale_type == RULER: # cut out ruler corner edges
        diff = half_h // 4
        half_w -= diff
        half_h -= diff

    return possible_scale[center_y - half_h:center_y + half_h,
                          center_x - half_w:center_x + half_w]


def resolve_ruler_metric(img):    
    _, th = cv2.threshold(img, 0, 255, cv2.THRESH_BINARY_INV+cv2.THRESH_OTSU)
    
    non_zero = cv2.countNonZero(th) 
    if non_zero > (th.shape[0]*th.shape[1] // 2): 
        th = np.invert(th)
    
    dil = cv2.dilate(th, cv2.getStructuringElement(cv2.MORPH_RECT, (1, 5)))
    contours, _ = cv2.findContours(dil, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    
    result = sort_and_filter_ruler_contours(contours, dil)
    if result is None:
        return None
    
    return result
    

def sort_and_filter_ruler_contours(contours, img):
    contours_with_stats = []
    for_mode = []
    
    for cnt in contours:
        (x, y), (_, _), angle_of_rotation = cv2.minAreaRect(cnt)   
        _, _, W, H = cv2.boundingRect(cnt)
        if H > W and W / H < 0.4:
            contours_with_stats.append((cnt, x, round(angle_of_rotation), H))
            for_mode.append(round(angle_of_rotation))
    
    if len(for_mode) == 0:
        return None
    mode = max(set(for_mode), key=for_mode.count) # compute mode of contour angles of rotation
    allowed = [x % 91 for x in range(mode-5, mode+6)]
    # keep only those with angle of rotation similar to mode
    grouped_by_angle = list(filter(lambda x: x[2] in allowed, contours_with_stats))
    
    if len(grouped_by_angle) < 8:
        return None

    sorted_left_to_right = sorted(grouped_by_angle, key=lambda x: x[1])
    
    # normalise the heights
    base = max(sorted_left_to_right, key=lambda x: x[3])[3]
    for i, (cnt, x, angle, h) in enumerate(sorted_left_to_right):
        sorted_left_to_right[i] = (cnt, x, angle, h / base)
    
    for n in range(100, 40, -5):
        upper = n / 100
        lower = (n - 5) / 100
        
        found_in_interval = []
        for i, (cnt, _, _, h) in enumerate(sorted_left_to_right):
            if upper >= h > lower:
                found_in_interval.append(i)

        if len(found_in_interval) > 1:
            result = check_ruler_scale(sorted_left_to_right, found_in_interval)
            if result is not None:
                fst, snd, units = result
                cntl = sorted_left_to_right[fst]
                cntr = sorted_left_to_right[snd]
                return cntl[0], cntr[0], units
    
    return None
        

def check_ruler_scale(contours, peaks):
    inbetween = []
    
    for i in range(len(peaks)-1):
        inbetween.append(peaks[i+1] - peaks[i]) # number of contours inbetween the two

    for i in range(len(inbetween)):
        c = 0 # number of higher contours between the two
        upper = contours[peaks[i]][3]
        for j in range(peaks[i], peaks[i+1]):
            if contours[j][3] > upper:
                c += 1
        if c > 1:
            continue
                
        if inbetween[i] == 8 and 16 in inbetween:
            return (peaks[i], peaks[i+1], "half in")
        if inbetween[i] == 8 and c == 1:
            return (peaks[i], peaks[i+1], "half in")
        if inbetween[i] == 8:
            return (peaks[i], peaks[i+1], "in")
        elif inbetween[i] == 16 and 8 in inbetween:
            return (peaks[i], peaks[i+1], "half in")
        elif inbetween[i] == 16:
            return (peaks[i], peaks[i+1], "in")
        elif inbetween[i] == 10:
            return (peaks[i], peaks[i+1], "cm")
        
    return None
    
    
def resolve_scale_bar_metric(img):
    w, h = img.shape[1], img.shape[0]
    _, th = cv2.threshold(img, 20, 255, cv2.THRESH_BINARY)
    
    contours, _ = cv2.findContours(th, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
    scale_bar = None
    
    for cnt in sorted(contours, key=lambda x: cv2.contourArea(x)):
        y, x, cnt_w, cnt_h = cv2.boundingRect(cnt)
        ratio_w = cnt_w / w
        ratio_h = cnt_h / h
        if 0.88 < ratio_w < 0.95 and 0.19 < ratio_h < 0.22:
            scale_bar = cnt

    if scale_bar is None:
        return None

    # leftmost pixel
    l_x, l_y = tuple(scale_bar[scale_bar[:, :, 0].argmin()][0])
    # rightmost pixel
    r_x, _ = tuple(scale_bar[scale_bar[:, :, 0].argmax()][0])
    scale_bar_pixel_length = r_x - l_x
    
    if l_y > h / 2:
        th = cv2.rotate(th, cv2.ROTATE_180)
        img = cv2.rotate(img, cv2.ROTATE_180)
        l_y = h - l_y
    
    text = image_to_string(th)
    number = ""
    units = ""
    for c in text:
        if c.isdigit():
            number += c
        elif c == "u":
            units += "μ"
        elif c.isalpha():
            units += c
    
    if number == "" or units == "":
        return None

    scale_bar_marked = mark_scale_bar(np.invert(th), l_x, r_x, l_y)
    return scale_bar_marked, units, scale_bar_pixel_length / int(number)


def mark_scale_bar(img, x1, x2, y):
    rgb_img = cv2.cvtColor(img, cv2.COLOR_GRAY2RGB)
    
    diff = img.shape[0] // 14
    
    rgb_img[y-diff:y+diff, x1-diff:x1+diff] = (0, 255, 0)
    rgb_img[y-diff:y+diff, x2-diff:x2+diff] = (0, 255, 0)
        
    return rgb_img


def mark_ruler_bar(img, cnt1, cnt2):
    rgb_img = cv2.cvtColor(img, cv2.COLOR_GRAY2RGB)
    
    for cnt in [cnt1, cnt2]:
        mask = np.zeros(img.shape,np.uint8)
        cv2.drawContours(mask, [cnt], 0, 255, -1)
        pixelpoints = cv2.findNonZero(mask)
        
        for pix in pixelpoints:
            x, y = pix[0][0], pix[0][1]
            rgb_img[y,x] = (0, 255, 0)
            
    return rgb_img  


def main():
    path = None
    if len(argv) > 1:
        if exists(argv[1]):
            path = argv[1]
    else:
        path = "images\\barbar_10.tif.png"
    
    if path:
        detector = ScaleDetector(path)
        result = detector.resolve_image_scale()
        if result is not None:
            scale_marked, units, pixels = result
            print("1" + str(units) + " = " + str(pixels) + "px")
        else:
            print("RESULT NOT FOUND")
    

if __name__ == "__main__":
    main()
